"use strict"

var hyperscript = require("./hyperscript")
var mountRedraw = require("./mount-redraw")
var domFor = require("./render/domFor")

var m = function m() { return hyperscript.apply(this, arguments) }
m.m = hyperscript
m.trust = hyperscript.trust
m.fragment = hyperscript.fragment
m.Fragment = "["
m.mount = mountRedraw.mount
m.render = require("./render")
m.redraw = mountRedraw.redraw
m.vnode = require("./render/vnode")
m.domFor = domFor.domFor

module.exports = m
